﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ImageRotateTest
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private static readonly Uri DefaultImageUri = new Uri("ms-appx:///Assets/lebowski.jpg");
        private static readonly ImageSource DefaultImageSource = new BitmapImage(DefaultImageUri);

        private static readonly Uri DefaultCdImageUri = new Uri("ms-appx:///Assets/cd_overlay.png");
        
        public MainPage()
        {
            InitializeComponent();

            Loaded += OnLoaded;
        }

        void OnLoaded(object sender, RoutedEventArgs e)
        {
            //Set the Image source
            var defaultCdImageSource = new BitmapImage(DefaultCdImageUri)
            {
                DecodePixelWidth = 300,
                DecodePixelHeight = 300
            };

            MainImage.Source = defaultCdImageSource;
            MainImage.ImageOpened += MainImageOnImageOpened;

            MainImageBrush.ImageSource = DefaultImageSource;
            MainImageBrush.ImageOpened += MainImageBrushOnImageOpened;
        }

        private void MainImageBrushOnImageOpened(object sender, RoutedEventArgs routedEventArgs)
        {
            EllipseRotateTransform.CenterX = MainPath.ActualHeight / 2;
            EllipseRotateTransform.CenterY = MainPath.Width / 2;
        }

        private void MainImageOnImageOpened(object sender, RoutedEventArgs routedEventArgs)
        {
            ImageRotateTransform.CenterX = MainImage.ActualHeight / 2;
            ImageRotateTransform.CenterY = MainImage.ActualWidth / 2;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void OnButtonRotateClick(object sender, RoutedEventArgs e)
        {
            ((Storyboard)Resources["RotationStoryboard"]).Begin();
            ((Storyboard)Resources["RotationStoryboardEllipse"]).Begin();
        }
    }
}
